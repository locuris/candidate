﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandidateTrait
{
    public string title;
    public Dictionary<ElectorateGroup, float> PopularityEffect;
}
