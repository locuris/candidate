﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Candidate
{
    public List<PolicyPromise> manifesto;
    public Dictionary<Issue, PoliticalStance> issueStances;
    public List<CandidateTrait> traits;

    public Dictionary<ElectorateGroup, float> PopularityEffect
    {
        get
        {
            Dictionary<ElectorateGroup, float> popEffects = new Dictionary<ElectorateGroup, float>();

            foreach(PolicyPromise pp in manifesto)
            {
                foreach(KeyValuePair<ElectorateGroup, float> popEff in pp.PopularityEffect)
                {
                    if (popEffects.ContainsKey(popEff.Key)) popEffects[popEff.Key] += popEff.Value;
                    else popEffects.Add(popEff.Key, popEff.Value);
                }                
            }

            foreach (KeyValuePair<Issue, PoliticalStance> issueStance in issueStances)
            {
                foreach (KeyValuePair<ElectorateGroup, float> electorateStance in issueStance.Key.electorateStances)
                {
                    if (popEffects.ContainsKey(electorateStance.Key))
                        popEffects[electorateStance.Key] += issueStance.Value.Effect(electorateStance.Value);
                    else popEffects.Add(electorateStance.Key, issueStance.Value.Effect(electorateStance.Value));
                }
            }

            foreach(CandidateTrait ct in traits)
            {
                foreach (KeyValuePair<ElectorateGroup, float> popEff in ct.PopularityEffect)
                {
                    if (popEffects.ContainsKey(popEff.Key)) popEffects[popEff.Key] += popEff.Value;
                    else popEffects.Add(popEff.Key, popEff.Value);
                }
            }

            return popEffects;
        }
    }

    public float VoteShare
    {
        get 
        {
            float voteShare = 0;
            foreach (KeyValuePair<ElectorateGroup, float> popEff in PopularityEffect)
            {
                voteShare += popEff.Key.influence * popEff.Value;
            }
            return voteShare;
        }
    }

}
