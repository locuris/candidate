﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Issue
{
    public string title;
    public string type1;
    public string type2;
    public string subtype1;
    public string subtype2;

    public Dictionary<ElectorateGroup, float> electorateStances;

}
