﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliticalStance : MonoBehaviour
{
    public string stance;
    public float modifier;

    public bool absolute;

    // Start is called before the first frame update
    void Start()
    {
        if (politicalStances == null)
            politicalStances = new List<PoliticalStance>();

        politicalStances.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float Effect(float baseValue)
    {
        if (absolute)
            return Mathf.Abs(baseValue) * modifier;
        else return baseValue * modifier;
    }

    private static List<PoliticalStance> politicalStances;

    public static List<PoliticalStance> Stances
    {
        get
        {
            return new List<PoliticalStance>(politicalStances);
        }
    }
}
