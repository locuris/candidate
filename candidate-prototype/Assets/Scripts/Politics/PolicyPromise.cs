﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolicyPromise
{
    private string title;

    private Dictionary<Issue, PoliticalStance> relatedIssues;

    public string Title
    {
        get
        {
            return title;
        }
    }

    public Dictionary<ElectorateGroup, float> PopularityEffect
    {
        get
        {
            Dictionary<ElectorateGroup, float> popEff = new Dictionary<ElectorateGroup, float>();
            foreach(KeyValuePair<Issue, PoliticalStance> issueStance in relatedIssues)
            {
                foreach(KeyValuePair<ElectorateGroup, float> electorateStance in issueStance.Key.electorateStances)
                {
                    if (popEff.ContainsKey(electorateStance.Key))
                        popEff[electorateStance.Key] += issueStance.Value.Effect(electorateStance.Value);
                    else popEff.Add(electorateStance.Key, issueStance.Value.Effect(electorateStance.Value));
                }
            }
            return popEff;
        }
    }
}