﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class Electorate : MonoBehaviour
{
    public string directory;
    public List<ElectorateGroup> groups;

    private void Start()
    {
        instance = this;

        groups = new List<ElectorateGroup>();

        StreamReader sr = new StreamReader(directory);

        while(!sr.EndOfStream)
        {
            string ln = sr.ReadLine();
            string[] eg = ln.Split(',');
            groups.Add(new ElectorateGroup(eg[0], eg[1]));
        }
    }

    private static Electorate instance;

    public static List<ElectorateGroup> Groups
    {
        get
        {
            return instance.groups;
        }
    }


}

[Serializable]
public class ElectorateGroup
{
    public string name;
    public float influence;    

    public ElectorateGroup(string name, string influence)
    {
        this.name = name;
        this.influence = float.Parse(influence);
    }
}